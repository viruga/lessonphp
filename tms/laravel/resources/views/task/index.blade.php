@extends('task.layouts.app')
@section('title')Список задач.@endsection
@section('content')
    <h1>Список задач.</h1>
    @if($tasks->isEmpty())
        <p>В настоящий момент, задач не найдето.</p>
    @endif
        <table>
            <tread>
                <tr>
                    <th>Title</th>
                    <th>Deadline</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </tread>
            <tbody>
                @foreach($tasks as $task)
                    <tr>
                        <td><a href="{{ route('task.show', $task->id) }}">{{ $task->title }}</a></td>
                        <td>{{ $task->deadline }}</td>
                        <td><a href="{{ route('task.edit', $task) }}">Редактировать</a></td>
                        <td>
                            <form action="{{ route('task.destroy', $task) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button
                                    type="submit"
                                    onclick="return confirm('Вы уверены что хотите удалить данное задание?!')">
                                    Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
@endsection
