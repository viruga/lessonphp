@extends('task.layouts.app')
@section('title')Добавление нового задания.@endsection
@section('content')
<h2>Создание задания</h2>
<form action="{{ route('task.store') }}" method="POST">
    @csrf
    <fieldset>
        <legend>Составление задания:</legend>
    <div>
        <label for="title">Title</label>
        <input type="text" id="title" name="title">
    </div>
    <div>
        <label for="description">Description</label>
        <input type="text" id="description" name="description">
    </div>
    <div>
        <label for="deadline">Deadline</label>
        <input type="datetime-local" id="deadline" name="deadline">
    </div>
    <button type="submit">Добавить</button>
    </fieldset>
</form>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@endsection
