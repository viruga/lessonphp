@extends('task.layouts.app')
@section('title')Редактируем задание: {{ $task->title }}.@endsection
@section('content')
    <form action="{{ route('task.update', $task) }}" method="POST">
        @csrf
        @method('PUT')
        <fieldset>
            <legend>Редактировать задание {{ $task->title }}:</legend>
            <div>
                <label>Title</label>
                <input type="text" name="title" value="{{ $task->title }}">
            </div>
            <div>
                <label>Description</label>
                <input type="text" name="description" value="{{ $task->description }}">
            </div>
            <div>
                <label>Deadline</label>
                <input type="datetime-local" name="deadline" value="{{ $task->deadline }}">
            </div>
            <button type="submit">Сохранить</button>
        </fieldset>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
