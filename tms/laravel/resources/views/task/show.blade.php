@extends('task.layouts.app')
@section('title')Просмотр задания: {{ $task->title }}.@endsection
@section('content')
    <h1>{{ $task->title }}</h1>
    <p>{{ $task->description }}</p>
    <p>Дата дедлайна: {{ $task->deadline }}</p>
    <a href="{{ route('task.edit', $task->id) }}">Редактирова</a>
@endsection
