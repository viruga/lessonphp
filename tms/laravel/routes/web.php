<?php
use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/task', [TaskController::Class, 'index'])->name('task.index');
Route::get('/task/crate', [TaskController::Class, 'create'])->name('task.create');
Route::post('/task', [TaskController::Class, 'store'])->name('task.store');
Route::get('/task/{task}/show', [TaskController::Class, 'show'])->name('task.show');
Route::get('/task/{task}/edit', [TaskController::Class, 'edit'])->name('task.edit');
Route::put('/task/{task}/', [TaskController::Class, 'update'])->name('task.update');
Route::delete('/task/{task}', [TaskController::Class, 'destroy'])->name('task.destroy');
