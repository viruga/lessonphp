CREATE TABLE students (
    id INT (10) NOT NULL AUTO_INCREMENT,
    name VARCHAR (50) NOT NULL,
    email VARCHAR (50) NOT NULL,
    PRIMARY key (id)
    );
INSERT INTO `students` (`id`, `name`, `email`) 
VALUES 
(NULL, 'Student1', 'email1@mail.ru'), 
(NULL, 'Student2', 'email2@mail.ru'), 
(NULL, 'Student3', 'email3@mail.ru'), 
(NULL, 'Student4', 'email4@mail.ru'), 
(NULL, 'Student5', 'email5@mail.ru');
CREATE TABLE enrollments (
    id INT (10) NOT NULL AUTO_INCREMENT,
    student_id INT (10) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (student_id) REFERENCES students (id) ON DELETE CASCADE
    );