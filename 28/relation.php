<?php
include_once "mod/connect.php";
include_once "mod/validate.php";
?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Связать БД</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>


<?php
function checked ($id,$student_id) {
    $result = '<td><input type="checkbox" name="students[]" value="'.$id.'"';
    if ($student_id != NULL) { $result = $result.' checked';}
    echo $result.'></td></tr>';
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $query = "DELETE FROM enrollments";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    if (isset($_POST['students'])) {
        $students = $_POST['students'];
        foreach ($students as $student) {
                echo "<br>Создаем связь:";
                $query = "INSERT INTO enrollments (student_id) VALUES (?)";
                $stmt = $conn->prepare($query);
                $stmt->bind_param('i', $student);
                if ($stmt->execute()) {
                    echo "<br>Ошибок нет. Создали связь с ID $student";
                }

            $stmt->close();
        }
    }
}

$query= "SELECT students.id, students.name, students.email, enrollments.student_id
FROM students
LEFT JOIN enrollments
ON students.id = enrollments.student_id";

$result = $conn->query($query);
if ($result->num_rows > 0) {
    echo '<form action="" method="post"><table class="table">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>E-mail</td>
                <td>Внести</td>
            </tr>';
    while ($row = $result->fetch_assoc()) {
        echo "<tr>
                    <td>$row[id]</td>
                    <td>$row[name]</td>
                    <td>$row[email]</td>";
        checked ($row['id'],$row['student_id']);
    }
    echo '</table><button type="submit" name="submit">Связать</button></form>';
}

$conn->close();
?>
</body>
</html>