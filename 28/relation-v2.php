<?php
include_once "mod/connect.php";
include_once "mod/validate.php";
?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Связать БД</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>


<?php
function checked ($id,$conn) {
    $query= "SELECT * FROM enrollments WHERE student_id = $id";
    $stmt = $conn->prepare($query);
    $stmt->execute();
        $result = '<td><input type="checkbox" name="students_hidden[]" type="hidden" hidden="hidden" value="'.$id.'" checked><input type="checkbox" name="students[]" value="'.$id.'"';
        if ($stmt->fetch()) { $result = $result.' checked';}
        echo $result.'></td></tr>';
    $stmt->close();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $query = "DELETE FROM enrollments";
    $stmt = $conn->prepare($query);
    $stmt->execute();
    if (isset($_POST['students'])) {
        $students = $_POST['students'];
        $students_hidden = $_POST['students_hidden'];
        foreach ($students_hidden as $student_hidden) {
            $query = "DELETE FROM enrollments WHERE student_id = ?";
            $stmt = $conn->prepare($query);
            $stmt->bind_param('i', $student_hidden);
            if ($stmt->execute()) {
            }
            $stmt->close();
        }
        foreach ($students as $student) {
                $query = "INSERT INTO enrollments (student_id) VALUES (?)";
                $stmt = $conn->prepare($query);
                $stmt->bind_param('i', $student);
                if ($stmt->execute()) {
                }
            $stmt->close();
        }
    }
}

$query= "SELECT * FROM students";
$result = $conn->query($query);
if ($result->num_rows > 0) {
    echo '<form action="" method="post"><table class="table">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>E-mail</td>
                <td>Внести</td>
            </tr>';
    while ($row = $result->fetch_assoc()) {
        echo "<tr>
                    <td>$row[id]</td>
                    <td>$row[name]</td>
                    <td>$row[email]</td>                    
                    ";

        checked ($row['id'],$conn);
    }
    echo '</table><button type="submit" name="submit">Связать</button></form>';
}

$conn->close();
?>

</body>
</html>