<?php
include_once "mod/connect.php";
include_once "mod/validate.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $user = validateText('user');
    $email = validateEmail('email');

    $errors = [];
    if ($user == false) {
        $errors[] = 'Проверьте Ваше имя';
    }
    if ($email == false) {
        $errors[] = 'Проверьте Ваш e-mail';
    }
    echo "Результат регистрации пользователя:<br>";
    if (!empty($errors)) {
        foreach ($errors as $error) {
            echo '<br>' . $error;
        }
    } else {
        $query = "INSERT INTO students (name, email) VALUES (?,?)";
        $stmt = $conn->prepare($query);
        $stmt->bind_param('ss', $user,$email);
        if ($stmt->execute()) {
            echo "Ошибок нет. Данные внесены в БД";
        }
        $stmt->close();

    }
}
$conn->close();
?>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Регистрация участников</title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
    <p><a href="relation.php" target="_blank" title="Связать таблички">Связать таблички</a>
        <form action="" method="post">
            <fieldset>
                <legend>Регистрация пользователя:</legend>
                <label for="name">Ваше имя:</label>
                <input name="user" id="name" type="text" placeholder="Имя пользователя">
                <br><label for="email">Ваш e-mail:</label>
                <input name="email" id="email" type="text" placeholder="E-mail пользователя">
                <p><button type="submit" name="submit">Зарегистрироваться</button>
                <button type="reset" name="reset">Очистить форму</button>
            </fieldset>
        </form>
    </body>
</html>
