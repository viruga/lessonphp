<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ApplicationController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/users', [UserController::Class, 'index'])->name('users.index');
Route::get('/users/create', [UserController::Class, 'create'])->name('users.create');
Route::post('/users/store', [UserController::Class, 'store'])->name('users.store');
Route::get('/users/edit/{user}', [UserController::Class, 'edit'])->name('users.edit');
Route::get('/users/delete/{user}', [UserController::class, 'delete'])->name('users.delete');
Route::post('/users/update/{user}', [UserController::Class, 'update'])->name('users.update');

Route::get('/applications', [ApplicationController::Class, 'index'])->name('applications.index');
Route::get('/applications/create', [ApplicationController::Class, 'create'])->name('applications.create');
Route::post('/applications/store', [ApplicationController::Class, 'store'])->name('applications.store');
Route::get('/applications/edit/{user}', [ApplicationController::Class, 'edit'])->name('applications.edit');
Route::get('/applications/delete/{user}', [ApplicationController::class, 'delete'])->name('applications.delete');
Route::post('/applications/update/{user}', [ApplicationController::Class, 'update'])->name('applications.update');
