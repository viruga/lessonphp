<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'status_id', 'title', 'description', 'password'];

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function status(){
        return $this->hasOne(Status::class, 'id', 'status_id');
    }
}

//select * from `users` where `users`.`application_id` = 1 and `users`.`application_id` is not null limit 1

