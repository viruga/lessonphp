<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Status;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    //
    public function index (){
        $applications = Application::all();

//        dd($applications);
        return view('applications.index', compact('applications'));
    }

    public function create() {
        return view('applications.create', [
            'users'=>User::all(),
            'statuses'=>Status::all(),
        ]);
    }

    public function store(Request $request) {
        $request->validate([
            'user_id'=> 'required',
            'status_id' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);
        Application::create($request->all());
        return redirect()->route('applications.index');
    }

    public function edit(string $id)
    {
        $users=User::all();
        $statuses=Status::all();
        $applications = Application::findOrFail($id);
        return view('applications.edit', compact('applications', 'users', 'statuses'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'user_id' => 'required',
            'status_id' => 'required',
        ]);
        $applications = Application::findOrFail($id);
        $applications->update($request->all());
        return redirect()->route('applications.index');
    }

    public function delete(string $id)
    {
        $application = Application::findOrFail($id);
        $application->delete();
        return redirect()->route('applications.index');
    }

}
