@extends('templates.app')
@section('title')Редактирование данных пользователя.@endsection
@section('content')
    <h2>Редактирование пользователя</h2>
    <form action="{{ route('users.update', $users, $users->id) }}" method="POST">
        @csrf
        <fieldset>
            <legend>Редактирование данных пользователя:</legend>
            <div>
                <label for="name">Name</label>
                <input type="text" id="name" name="name" value="{{ $users->name }}">
            </div>
            <div>
                <label for="email">E-mail</label>
                <input type="text" id="email" name="email" value="{{ $users->email }}">
            </div>
            <button type="submit">Изменить</button>
        </fieldset>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
