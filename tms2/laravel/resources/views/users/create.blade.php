@extends('templates.app')
@section('title')Добавить нового пользователя.@endsection
@section('content')
    <h2>Создание пользователя</h2>
    <form action="{{ route('users.store') }}" method="POST">
        @csrf
        <fieldset>
            <legend>Добавление нового пользователя:</legend>
            <div>
                <label for="name">Name</label>
                <input type="text" id="name" name="name">
            </div>
            <div>
                <label for="email">E-mail</label>
                <input type="text" id="email" name="email">
            </div>
            <div>
                <label for="password">Password</label>
                <input type="text" id="password" name="password">
            </div>
            <button type="submit">Добавить</button>
        </fieldset>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
