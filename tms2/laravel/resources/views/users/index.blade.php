@extends('templates.app')
@section('title')Список пользователей.@endsection
@section('content')
    @if($users->isEmpty())
        <p>В настоящий момент, пользователей не найдено.</p>
        <p><a href="{{ route('users.create') }}">Создать нового пользователя.</a></p>
    @else
        <table>
            <thead>
            <tr>
                <th>Имя польователя</th>
                <th>E-mail</th>
                <th>Удалить пользователя</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td><a href="{{ route('users.edit', $user->id) }}">{{ $user->name }}</a></td>
                    <td>{{ $user->email }}</td>
                    <td><a href="{{ route('users.delete', $user->id) }}">Удалить</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <p><a href="{{ route('users.create') }}">Создать нового пользователя.</a></p>
    @endif
@endsection
