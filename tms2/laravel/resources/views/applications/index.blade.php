@extends('templates.app')
@section('title')Список Заявок.@endsection
@section('content')
    @if($applications->isEmpty())
        <p>В настоящий момент, заявок не найдено.</p>
    @else
        <table>
            <thead>
                <tr>
                    <th>Наименование заявки</th>
                    <th>Описание</th>
                    <th>Статус заявки</th>
                    <th>Ответственный</th>
                    <th>Удалить</th>
                </tr>
            </thead>
            <tbody>
                @foreach($applications as $application)
                    <tr>
                        <td><a href="{{ route('applications.edit', $application->id) }}">{{ $application->title }}</a></td>
                        <td>{{ $application->description }}</td>
                        <td>{{ $application->status['status']}}</td>
                        <td>{{ $application->user['name']}}</td>
                        <td><a href="{{ route('applications.delete', $application->id) }}">Удалить</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
    <p><a href="{{ route('applications.create') }}">Создать новую заявку.</a></p>
@endsection
