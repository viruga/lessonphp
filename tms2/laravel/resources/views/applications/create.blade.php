@extends('templates.app')
@section('title')Добавить новую заявку.@endsection
@section('content')
    <h2>Создание новой заявки</h2>
    <form action="{{ route('applications.store') }}" method="POST">
        @csrf
        <fieldset>
            <legend>Добавить новую заявку:</legend>
            <div>
                <label for="title">Наименование заявки</label>
                <input type="text" id="title" name="title">
            </div>
            <div>
                <label for="description">Описание заявки</label>
                <input type="text" id="description" name="description">
            </div>
            <div>
                <label for="user_id">Создал пользователь:</label>
                <select name="user_id" id="user_id">
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
                <label for="status_id">Начальный статус:</label>
                <select name="status_id" id="status_id">
                    @foreach($statuses as $status)
                        <option value="{{ $status->id }}">{{ $status->status }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit">Добавить</button>
        </fieldset>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
