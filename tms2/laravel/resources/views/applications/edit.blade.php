@extends('templates.app')
@section('title')Редактирование заявки.@endsection
@section('content')
    <h2>Редактирование заявки</h2>
    <form action="{{ route('applications.update', $applications, $applications->id) }}" method="POST">
        @csrf
        <fieldset>
            <legend>Редактирование данных заявки:</legend>
            <div>
                <label for="name">Наименование</label>
                <input type="text" id="name" name="title" value="{{ $applications->title }}">
            </div>
            <div>
                <label for="email">Описание</label>
                <input type="text" id="email" name="description" value="{{ $applications->description }}">
            </div>
            <div>
                <label for="user_id">Создал пользователь:</label>
                <select name="user_id" id="user_id">
                    @foreach($users as $user)
                        <option value="{{ $user->id }}"
                        @if($applications->user['name'] === $user->name)
                            selected
                        @endif
                        >{{ $user->name }}</option>
                    @endforeach
                </select>
                <label for="status_id">статус:</label>
                <select name="status_id" id="status_id">
                    @foreach($statuses as $status)
                        <option value="{{ $status->id }}"
                            @if($applications->status['status'] === $status->status)
                                    selected
                            @endif
                        >{{ $status->status }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit">Изменить</button>
        </fieldset>
    </form>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
@endsection
