<?php
if ($_SERVER['REQUEST_METHOD'] == 'GET' and  (isset($_GET['action']) and ($_GET['action'] == 'edit'))) {
    if (validateInt($_GET['id']) == true) {
        $stmt = $db->prepare("SELECT * FROM users WHERE id=$_GET[id]");
        if ($stmt->execute()){
            $result = $stmt->get_result();
            echo '<form action="" method="post">
                    <fieldset>';
            while ($row = $result->fetch_assoc()) {
                echo '  <legend>Редактирование данных пользователя:</legend>
                        <label for="id">Редактируемый ID:</label>
                        <input name="id" id="id" type="text" value="'.$row['id'].'" readonly>
                        <br><label for="name">Ваше имя:</label>
                        <input name="user" id="name" type="text" value="'.$row['name'].'">
                        <br><label for="age">Ваш возраст:</label>
                        <input name="age" id="age" type="text" value="'.$row['age'].'">
                        <br><label for="email">Ваш e-mail:</label>
                        <input name="email" id="email" type="text" value="'.$row['email'].'">
                        <p><button type="submit" name="submit">Изменить</button>';
            }
            echo '    </fieldset>
                    </form>';
        $stmt->close();
        }
    }
    else {
        echo "ID элемента для редактирования может быть, только целочисленный";
    }
}


?>