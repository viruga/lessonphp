<?php
include_once "mod/connect.php";
include_once "mod/show_user.php";
include_once "mod/validate.php";
?>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Регистрация участников</title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <form action="" method="post">
            <fieldset>
                <legend>Регистрация пользователя:</legend>
                <label for="name">Ваше имя:</label>
                <input name="user" id="name" type="text" placeholder="Имя пользователя">
                <br><label for="age">Ваш возраст:</label>
                <input name="age" id="age" type="text" placeholder="Возраст пользователя">
                <br><label for="email">Ваш e-mail:</label>
                <input name="email" id="email" type="text" placeholder="E-mail пользователя">
                <p><button type="submit" name="submit">Зарегистрироваться</button>
                <button type="reset" name="reset">Очистить форму</button>
            </fieldset>
        </form>
        <form action="" method="post">
            <fieldset>
                <legend>Поиск пользователя:</legend>
                <label for="search">Поиск:</label>
                <input name="search" id="search" type="text" placeholder="Поиск"></label>
                <label for="select">По:</label>
                <select id="select" name="type">
                    <option value="id">ID</option>
                    <option value="name">Имя</option>
                    <option value="age">Возраст</option>
                    <option value="email">Почта</option>
                </select>
                <button type="submit" name="submit">Поиск</button>
            </fieldset>
        </form>
    <?php
    include_once "register.php";
    include_once "list_user.php";
    include_once "delete.php";
    include_once "edit.php";
    $query = "SELECT * FROM users ORDER BY id";
    showUser ($query,$db);
    $db->close();
    ?>
    </body>
</html>
