<?php
function insertToDb ($user,$age,$email,$db) {
    $query = "INSERT INTO users (name, age, email) VALUES (?,?,?)";
    $stmt = $db->prepare($query);
    $stmt->bind_param('sis', $user,$age,$email);
    if ($stmt->execute()) {
        echo "Ошибок нет. Данные внесены в БД";
    }
    $stmt->close();
}
function editUser ($id,$user,$age,$email,$db) {
    $query = "UPDATE users SET name = ?, age = ?, email = ? WHERE id = ?";
    $stmt = $db->prepare($query);
    $stmt->bind_param('sisi', $user,$age,$email,$id);
    if ($stmt->execute()) {
        echo "Ошибок нет. Данные изменены в БД";
    }
    $stmt->close();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' and  (isset($_POST['user']) or isset($_POST['age']) or isset($_POST['email']))) {
    $user = validateText('user');
    $age = validateInt($_POST['age']);
    $email = validateEmail('email');

    $errors = [];
    if ($user == false) {
        $errors[] = 'Проверьте Ваше имя';
    }
    if ($age == false) {
        $errors[] = 'Проверьте Ваш возраст';
    }
    if ($email == false) {
        $errors[] = 'Проверьте Ваш e-mail';
    }
    echo "Результат регистрации пользователя:<br>";
    if (!empty($errors)) {
        foreach ($errors as $error) {
            echo '<br>' . $error;
        }
    } else {
        if (isset($_POST['id']) and validateInt($_POST['id'])) {
            editUser ($_POST['id'],$user,$age,$email,$db);
        } else {
            insertToDb ($user,$age,$email,$db);
            $query = "SELECT * FROM users ORDER BY id DESC LIMIT 1";
            echo "<p>Вы зарегистировали:";
            showUser ($query,$db);
        }

    }

}
