<?php
include_once "mod/connect.php";
class Users {
    private $tableName = 'users';
    private $conn;
    private string $name;
    private int $age;
    private string $email;
    public function __construct($name,$age,$email,$conn)
    {
        $this->name=$name;
        $this->age=$age;
        $this->email=$email;
        $this->conn=$conn;
    }
    public function addUser()
    {
        $query = "INSERT INTO $this->tableName (name, age, email) VALUES (:name, :age, :email)";
        $stmt = $this->conn->prepare($query);
        var_dump($stmt);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':age', $this->age);
        $stmt->bindParam(':email', $this->email);
        $stmt->execute();
        return $stmt;
    }
    public function viewUsers() {
        $query = "SELECT * FROM $this->tableName";
        $stmt = $this->conn->query($query);
        while ($row = $stmt->fetch()) {
            echo "<p>ID: ". $row['id'] . " - ";
            echo "Name: ". $row['name'] . " - ";
            echo "Age: ". $row['age'] . " - ";
            echo "Email: ". $row['age'];
        }
    }
    public function editName($idUser, $newName) {
        $query = "UPDATE $this->tableName SET name = :name WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':name', $newName);
        $stmt->bindParam(':id', $idUser);
        $stmt->execute();
    }
    public function deleteUser ($idUser){
        $query = "DELETE FROM $this->tableName WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id', $idUser);
        $stmt->execute();
    }

}
$db = new Database();
$db = $db->getConnection();

$user = new Users('Валера', 23, '111@mail.ru', $db);
$user->addUser();
$user->viewUsers();
$user->editName(9,'Павел');
$user->deleteUser(35);

?>