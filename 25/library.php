<?php
class Library{
    private array $books= [];

    public function setBooks(string $books): void
    {
        $this->books = $books;
    }

    public function getBooks(): string
    {
        return $this->books;
    }

    public function addBook (Book $book){
        $this->books[]=$book;
    }

    public function removeBook ($title) {
        foreach ($this->books as $key=>$book) {
            if ($book->title === $title) {
                unset ($this->books[$key]);
            }
        }
    }

//  Второе решение данного метода, но мне не понравилось, т.к. не удаляет объект
//    public function removeBook ($title){
//        return array_filter($this->books, function ($item) use ($title){
//            echo "<br>$item->title - $title";
//            var_dump ($item->title != $title);
//           return $item->title != $title;
//        });
//    }

    public function getBooksNew() {
        echo "<pre>";
        var_dump($this->books);
        echo "</pre>";
    }

    public function getAvailableBooks() {
        return array_filter($this->books, function ($item){
           return $item->is_available === true;
        });
    }

    public function getBookByTitle($title) {
        return array_filter($this->books, function ($item) use ($title){
           return $item->title == $title;
        });
    }

    public function getBooksByAuthor(Author $author) {
        $author =  $author->getId();
        return array_filter($this->books, function ($item) use ($author){
            return $item->author_id == $author;
        });
    }
}


?>