<?php
include_once "book.php";
include_once "author.php";
include_once "library.php";

$tolkin = new Author(1,'Толкин');
$dostoevski = new Author(2, 'Достоевский');
$pushkin = new Author(3, 'Пушкин');

$library = new Library();
$library->addBook(new Book('Властелин колец', 1, true));
$library->addBook(new Book('Хоббит', 1, false));
$library->addBook(new Book('Идиот', 2, true));
$library->addBook(new Book('Преступление и наказание', 2, true));
$library->addBook(new Book('Бедные люди', 2, true));
$library->addBook(new Book('Евгений Онегин', 3, true));
$library->addBook(new Book('Пиковая дама', 3, true));

$library->removeBook('Пиковая дама');

$library->getBooksNew();

echo "<pre>";
var_dump($library->getAvailableBooks());

var_dump($library->getBookByTitle('Хоббит'));

var_dump($library->getBooksByAuthor($dostoevski));
echo "</pre>";


//Система управления библиотекой. Для этого нам потребуются три класса: Book, Author и Library.
//
//Класс Book будет иметь следующие свойства:
//
//title
//author_id
//is_available
//
//
//Класс Author будет иметь следующие свойства:
//
//id
//last_name
//
//Класс Library будет иметь следующие свойства:
//
//books
//
//
//Теперь, нам необходимо реализовать  методы в классе Library. Например:
//
//addBook(Book $book) - добавляет книгу в библиотеку
//removeBook(Book $book) - удаляет книгу из библиотеки
//getBooks() - возвращает все книги из библиотеки
//getAvailableBooks() - возвращает все доступные книги из библиотеки
//getBookByTitle($title) - возвращает книгу по ее названию
//getBooksByAuthor(Author $author) - возвращает все книги автора

?>
