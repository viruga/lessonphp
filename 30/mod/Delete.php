<?php
class Delete {
    private int $id;
    private $conn;
    public function __construct($id,$conn)
    {
        $this->id=$id;
        $this->conn=$conn;
    }
    public function getDelete (){
        $query = "DELETE FROM users WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();
    }
}
