<?php
class Edit {
    private int $id;
    private $conn;
    public function __construct($id,$conn)
    {
        $this->id=$id;
        $this->conn=$conn;
    }
    public function getEditName ($newName){
        $query = "UPDATE users SET name = :name WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':name', $newName);
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();
    }
    public function getEditAge ($newAge){
        $query = "UPDATE users SET age = :age WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':age', $newAge);
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();
    }
    public function getEditEmail ($newEmail){
        $query = "UPDATE users SET email = :email WHERE id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':email', $newEmail);
        $stmt->bindParam(':id', $this->id);
        $stmt->execute();
    }

}