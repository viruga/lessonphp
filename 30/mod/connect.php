<?php
class Database {
    private $dbhost = 'localhost';
    private $dbname = 'tms';
    private $dbuser = 'root';
    private $dbpassword = '';
    public function getConnection()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->dbhost;dbname=$this->dbname", $this->dbuser,$this->dbpassword);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }   catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
        return $this->conn;
    }
}

?>