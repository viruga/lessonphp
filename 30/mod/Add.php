<?php
class User {
    private $conn;
    private string $name;
    private int $age;
    private string $email;
    public function __construct($name,$age,$email,$conn)
    {
        $this->name=$name;
        $this->age=$age;
        $this->email=$email;
        $this->conn=$conn;
    }
    public function addUser()
    {
        $query = "INSERT INTO users (name, age, email) VALUES (:name, :age, :email)";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':age', $this->age);
        $stmt->bindParam(':email', $this->email);
        $stmt->execute();
        return $stmt;
    }
}

?>