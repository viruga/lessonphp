<?php
class View {
    private string $nameDb;
    private $conn;
    public function __construct($nameDb,$conn)
    {
        $this->conn=$conn;
        $this->nameDb=$nameDb;
    }
    public function viewAll () {
        $query = "SELECT * FROM $this->nameDb";
        $stmt = $this->conn->query($query);
        while ($row = $stmt->fetch()) {
            echo "<p>ID: ". $row['id'] . " - ";
            echo "Name: ". $row['name'] . " - ";
            echo "Age: ". $row['age'] . " - ";
            echo "Email: ". $row['email'];
        }
    }
    public function viewCount () {
        $query = "SELECT count(*) FROM $this->nameDb";
        $stmt = $this->conn->query($query);
        $row = $stmt->fetch();
        echo "<p>Всего пользователей в таблице: $row[0]";
    }
}
