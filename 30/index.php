<?php
include_once "mod/connect.php";
include_once "mod/Delete.php";
include_once "mod/Edit.php";
include_once "mod/View.php";
include_once "mod/Add.php";

$db = new Database();
$db = $db->getConnection();

$user = new User ('New', 55, '55@mail.ru', $db);
$user->addUser();

$userView = new View ('users',$db);
$userView->viewAll();
$userView->viewCount();

$userDelete = new Delete(37,$db);
$userDelete->getDelete();

$editUser = new Edit(31,$db);
$editUser->getEditName('Павел');
$editUser->getEditAge(25);
$editUser->getEditEmail('233@mail.ru');

?>