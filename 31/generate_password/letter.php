<?php
function letter () {
    $letterList = "abcdefghijklmnopqrstuvwxyz";
    $randLetter = rand(0,25);
    $result = mb_substr($letterList,$randLetter,1);
    $randUppercase = rand(0,1);
    if ($randUppercase == '1') {
        $result = uppercase ($result);
    }
    return $result;
}

function uppercase ($letter) {
    return strtoupper ($letter);
}
?>