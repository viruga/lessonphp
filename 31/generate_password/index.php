<?php
include_once "number.php";
include_once "letter.php";
include_once "symbol.php";
function generatePassword ($number) {
    $newPassword = "";
    for ($i=0;$i<$number;$i++) {
        $choiceMethod = rand(0,2);
        switch ($choiceMethod) {
            case 0:
                $newPassword = $newPassword.number();
                break;
            case 1:
                $newPassword = $newPassword.letter();
                break;
            case 2:
                $newPassword = $newPassword.symbol();
                break;
        }
    }
    return $newPassword;
}
$password = generatePassword(25);
echo $password;
?>

